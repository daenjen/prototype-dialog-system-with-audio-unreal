// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class DialogSystemEditorTarget : TargetRules
{
    public DialogSystemEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.Add("DialogSystem");

        // Game editor
        ExtraModuleNames.AddRange(new string[]
            {
                "DialogSystemEditor"
            });
    }
}
