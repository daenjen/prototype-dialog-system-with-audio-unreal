// Copyright Epic Games, Inc. All Rights Reserved.

#include "DialogSystemEditor.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FDialogSystemEditorModule, DialogSystemEditor);
 
DEFINE_LOG_CATEGORY(DialogSystemEditor)

#define LOCTEXT_NAMESPACE "DialogSystemEditor"

void FDialogSystemEditorModule::StartupModule()
{
	UE_LOG(DialogSystemEditor, Warning, TEXT("DialogSystemEditor: Log Started"));
}

void FDialogSystemEditorModule::ShutdownModule()
{
	UE_LOG(DialogSystemEditor, Warning, TEXT("DialogSystemEditor: Log Ended"));
}

#undef LOCTEXT_NAMESPACE