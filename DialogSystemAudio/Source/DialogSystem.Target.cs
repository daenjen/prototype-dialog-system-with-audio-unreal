// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class DialogSystemTarget : TargetRules
{
    public DialogSystemTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.Add("DialogSystem");

        if (bBuildEditor)
        {
            ExtraModuleNames.AddRange(
                new string[]
           {
                "DialogSystemEditor"
           });
        }
    }
}
