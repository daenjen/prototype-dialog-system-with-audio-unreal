// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "DialogSystem/Data/FSubtitle.h"
#include "FDialog.generated.h"


USTRUCT(BlueprintType)
struct DIALOGSYSTEM_API FDialog : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	//The question expected
	UPROPERTY(EditAnywhere)
		FString Question;

	//The audio voice to play
	UPROPERTY(EditAnywhere)
		USoundBase* SFXVoice;

	//Array of subtitles
	UPROPERTY(EditAnywhere)
		TArray<FSubtitle> Subtitles;

	//Bool check for the NPC
	UPROPERTY(EditAnywhere)
		bool bNPCAnswer;
};
