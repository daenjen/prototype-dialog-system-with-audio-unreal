// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "FSubtitle.generated.h"


USTRUCT(BlueprintType)
struct DIALOGSYSTEM_API FSubtitle : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	//The subtitle that will be displayed  
	UPROPERTY(EditAnywhere)
		FString Subtitle;

	//Relative time for the subtitle to appear
	UPROPERTY(EditAnywhere)
		float SubtitleTime;
};
