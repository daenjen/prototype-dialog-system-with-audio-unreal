// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogSystem/GenericGraph/DialogueSession.h"
#include "DialogSystem/GenericGraph/DialogueSessionNode.h"
#include "DialogSystem/GenericGraph/DialogueSessionEdge.h"

#define LOCTEXT_NAMESPACE "DialogueSession"

UDialogueSession::UDialogueSession()
{
	NodeType = UDialogueSessionNode::StaticClass();
	EdgeType = UDialogueSessionEdge::StaticClass();

	LeftDialoguerBgColor = FLinearColor::Black;
	RightDialoguerBgColor = FLinearColor(0.93f, 0.93f, 0.93f, 1.f);

	Name = "DialogueSession";
}

#undef LOCTEXT_NAMESPACE