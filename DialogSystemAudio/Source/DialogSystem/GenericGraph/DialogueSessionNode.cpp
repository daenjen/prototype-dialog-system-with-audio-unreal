// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogSystem/GenericGraph/DialogueSessionNode.h"
#include "DialogSystem/GenericGraph/DialogueSession.h"


#define LOCTEXT_NAMESPACE "DialogueSessionNode"

UDialogueSessionNode::UDialogueSessionNode()
{
#if WITH_EDITORONLY_DATA
	CompatibleGraphType = UDialogueSession::StaticClass();

	ContextMenuName = LOCTEXT("ContextMenuName", "Dialogue Session Node");
#endif
}

#if WITH_EDITOR

FText UDialogueSessionNode::GetNodeTitle() const
{
	return Paragraph.IsEmpty() ? LOCTEXT("EmptyParagraph", "(Empty paragraph)") : Paragraph;
}

void UDialogueSessionNode::SetNodeTitle(const FText& NewTitle)
{
	Paragraph = NewTitle;
}

FLinearColor UDialogueSessionNode::GetBackgroundColor() const
{
	UDialogueSession* graph = Cast<UDialogueSession>(GetGraph());

	if (graph == nullptr)
		return Super::GetBackgroundColor();

	switch (DialoguerPostion)
	{
	case EDialoguerPostion::Left:
		return graph->LeftDialoguerBgColor;

	case EDialoguerPostion::Right:
		return graph->RightDialoguerBgColor;

	default:
		return FLinearColor::Black;
	}
}

#endif

#undef LOCTEXT_NAMESPACE