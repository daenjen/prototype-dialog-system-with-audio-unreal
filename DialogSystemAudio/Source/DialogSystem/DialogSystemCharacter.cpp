// Copyright Epic Games, Inc. All Rights Reserved.

#include "DialogSystemCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Components/AudioComponent.h"
#include "GameFramework/SpringArmComponent.h"

//////////////////////////////////////////////////////////////////////////
// ADialogSystemCharacter

ADialogSystemCharacter::ADialogSystemCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//Default bools
	bIsTalking = false;
	bIsInTalkRange = false;
	NPCCharacter = nullptr;

	//Create an audio component
	AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
	AudioComp->SetupAttachment(RootComponent);
}

//////////////////////////////////////////////////////////////////////////
// Input

void ADialogSystemCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ADialogSystemCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ADialogSystemCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ADialogSystemCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ADialogSystemCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ADialogSystemCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ADialogSystemCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ADialogSystemCharacter::OnResetVR);

	//Input to start the conversation with the NPC
	PlayerInputComponent->BindAction("Talk", IE_Pressed, this, &ADialogSystemCharacter::TalkToNPC);
}

void ADialogSystemCharacter::OnResetVR()
{
	// If DialogSystem is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in DialogSystem.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ADialogSystemCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void ADialogSystemCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void ADialogSystemCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ADialogSystemCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ADialogSystemCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && !bIsTalking)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ADialogSystemCharacter::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && !bIsTalking)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ADialogSystemCharacter::TalkToNPC()
{
	if (bIsInTalkRange)
	{
		bIsTalking = !bIsTalking;

		//Toggle the UI Dialog
		ToggleUI();

		if (bIsTalking && NPCCharacter)
		{
			//Get NPC location
			FVector Location = NPCCharacter->GetActorLocation();
			//Get our location
			FVector TargetLocation = GetActorLocation();
			//Set our rotation to the NPC direction
			NPCCharacter->SetActorRotation((TargetLocation - Location).Rotation());
		}
	}
}

void ADialogSystemCharacter::GeneratePlayerLines(UDataTable& PlayerLines)
{
	//Get all row names of the table
	TArray<FName> PlayerOptions = PlayerLines.GetRowNames();

	for (auto It : PlayerOptions)
	{
		//Retrieve content of the table
		FDialog* Dialog = SearchDialog(&PlayerLines, It);

		if (Dialog)
		{
			//Add our questions from the table
			Questions.Add(Dialog->Question);
		}
	}

	//Reference of the player lines
	AvailableLines = &PlayerLines;
}


void ADialogSystemCharacter::Talk(FString MyQuestions, TArray<FSubtitle>& Subtitles)
{
	//Get all row names of the table available for the player
	TArray<FName> PlayerOptions = AvailableLines->GetRowNames();

	for (auto It : PlayerOptions)
	{
		//Retrieve content of the table
		FDialog* Dialog = SearchDialog(AvailableLines, It);

		if (Dialog && Dialog->Question == MyQuestions)
		{
			//Assign the corresponding sound to the dialog found and play it
			AudioComp->SetSound(Dialog->SFXVoice);
			AudioComp->Play();

			//Update the current subtitles
			Subtitles = Dialog->Subtitles;

			if (MyUI && NPCCharacter && Dialog->bNPCAnswer)
			{
				//Get reference of out array of subtitles
				TArray<FSubtitle> SubtitlesToDisplay;

				//Create a float for the subtitles time to display
				float TotalSubsTime = 0.f;

				//Give for every subtitles a time to display
				for (int32 i = 0; i < Subtitles.Num(); i++)
				{
					//Set the subtitles time
					TotalSubsTime += Subtitles[i].SubtitleTime;
				}

				//Negate to NPC to answer after the subs
				TotalSubsTime += 1;//TimeBeforeNPCTalk; //1

				//Let our NPC responde after the specific time
				NPCCharacter->AnswerToCharacter(It, SubtitlesToDisplay, TotalSubsTime);
			}
			else if (!Dialog->bNPCAnswer)
			{
				TalkToNPC();
				break;
			}
		}
	}
}

FDialog* ADialogSystemCharacter::SearchDialog(UDataTable* TableToSearch, FName RowName)
{
	//Check for the table
	if (!TableToSearch)
		return nullptr;

	//Return the given row
	FString ContextString;
	return TableToSearch->FindRow<FDialog>(RowName, ContextString);
}
