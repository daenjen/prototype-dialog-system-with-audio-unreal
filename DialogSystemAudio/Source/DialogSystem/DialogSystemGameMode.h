// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DialogSystemGameMode.generated.h"

UCLASS(minimalapi)
class ADialogSystemGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADialogSystemGameMode();
};



