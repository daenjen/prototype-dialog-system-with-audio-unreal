// Fill out your copyright notice in the Description page of Project Settings.


#include "DialogSystem/NPC/NPCCharacter.h"
#include "DialogSystem/DialogSystemCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

// Sets default values
ANPCCharacter::ANPCCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Initialize the box collider component
	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	BoxComp->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	//Initialize audio components
	AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
	AudioComp->SetupAttachment(RootComponent);

	////Register the begin and end overlap functions
	BoxComp->OnComponentBeginOverlap.AddDynamic(this, &ANPCCharacter::OnBoxOverlapBegin);
	BoxComp->OnComponentEndOverlap.AddDynamic(this, &ANPCCharacter::OnBoxOverlapEnd);
}

// Called to bind functionality to input
void ANPCCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


// Called when the game starts or when spawned
void ANPCCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ANPCCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ANPCCharacter::AnswerToCharacter(FName PlayerLine, TArray<FSubtitle>& SubtitlesToDisplay, float Delay)
{
	//Check for the npc dialog
	if (!NPCLines)
		return;

	//Retrieve the corresponding dialog line
	FString DialogString;
	FDialog* Dialog = NPCLines->FindRow<FDialog>(PlayerLine, DialogString);

	//Get player reference
	ADialogSystemCharacter* MyPlayer = Cast<ADialogSystemCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	if (Dialog && MyPlayer)
	{
		//Timers for the subtitles
		FTimerHandle TimerHandle;
		FTimerDelegate TimerDelegate;

		//Bind to the npc talk function
		TimerDelegate.BindUFunction(this, FName("Talk"), Dialog->SFXVoice, Dialog->Subtitles);

		//Play the timer
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, Delay, false);
	}
}

void ANPCCharacter::Talk(USoundBase* SFX, TArray<FSubtitle> Subs)
{
	//Get player reference
	ADialogSystemCharacter* MyPlayer = Cast<ADialogSystemCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	//Assign the corresponding sound to the dialog found and play it
	AudioComp->SetSound(SFX);
	AudioComp->Play();

	//Update the current subtitle with the new one
	MyPlayer->GetUI()->UpdateSubtitles(Subs);
}


void ANPCCharacter::OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA<ADialogSystemCharacter>())
	{
		//Get player reference
		ADialogSystemCharacter* MyPlayer = Cast<ADialogSystemCharacter>(OtherActor);

		//Set player variables for the dialog
		MyPlayer->SetTalkRange(true);
		MyPlayer->GeneratePlayerLines(*PlayerLines);
		MyPlayer->SetNPC(this);
	}
}

void ANPCCharacter::OnBoxOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor->IsA<ADialogSystemCharacter>())
	{
		//Get player reference
		ADialogSystemCharacter* MyPlayer = Cast<ADialogSystemCharacter>(OtherActor);

		//Reset player variables for the dialog
		MyPlayer->SetTalkRange(false);
		MyPlayer->SetNPC(nullptr);
	}
}

