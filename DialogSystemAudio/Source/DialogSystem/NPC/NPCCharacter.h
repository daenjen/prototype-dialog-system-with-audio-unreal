// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DialogSystem/Data/FSubtitle.h"
#include "Engine/DataTable.h"
#include "Components/BoxComponent.h"
#include "NPCCharacter.generated.h"

UCLASS()
class DIALOGSYSTEM_API ANPCCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ANPCCharacter();


public:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

public:
	//Answers to the character after a specified delay
	void AnswerToCharacter(FName PlayerLine, TArray<FSubtitle>& SubtitlesToDisplay, float Delay);

	//Retrieves the corresponding character lines
	UDataTable* GetPlayerLines() { return PlayerLines; }

protected:
	UPROPERTY(VisibleAnywhere)
		class UBoxComponent* BoxComp;

	UPROPERTY(VisibleAnywhere)
		UAudioComponent* AudioComp;

	UPROPERTY(EditAnywhere, Category = "DialogSystem")
		UDataTable* PlayerLines;

	UPROPERTY(EditAnywhere, Category = "DialogSystem")
		UDataTable* NPCLines;

private:
	UFUNCTION()
		void OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnBoxOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void Talk(USoundBase* SFX, TArray<FSubtitle> Subs);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
