// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DialogSystem/Data/FSubtitle.h"
#include "DialogUI.generated.h"


UCLASS()
class DIALOGSYSTEM_API UDialogUI : public UUserWidget
{
	GENERATED_BODY()

public:
	//Subtitle text display in game
	UPROPERTY(BlueprintReadOnly)
		FString SubtitleToDisplay;

	//This array will populate our buttons from within the show function
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray<FString> Questions;

public:
	//Update the current subtitle
	UFUNCTION(BlueprintCallable, Category = "DialogSystem")
		void UpdateSubtitles(TArray<FSubtitle> Subtitles);

	//Adds the widget to our viewport and populates the buttons with the given questions
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "DialogSystem")
		void Show();
};

class UMGAsyncTask :public FNonAbandonableTask
{
	//Subtitles to display
	TArray<FSubtitle> Subs;

	//Reference of UI
	UDialogUI* DialogUI;

public:
	//Constructor class
	UMGAsyncTask(TArray<FSubtitle>& Subs, UDialogUI* DialogUI)
	{
		this->Subs = Subs;
		this->DialogUI = DialogUI;
	}

	//Function needed by the UE in order to determine what's the tasks' status
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(UMGAsyncTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	void DoWork()
	{
		for (int32 i = 0; i < Subs.Num(); i++)
		{
			//Sleep means that we pause this thread for the given time
			FPlatformProcess::Sleep(Subs[i].SubtitleTime);

			//Update our subtitles after the thread comes back
			DialogUI->SubtitleToDisplay = Subs[i].Subtitle;
		}

		//Sleep 1 second to let the user read the text
		FPlatformProcess::Sleep(1.f);

		//Clear subtitle
		DialogUI->SubtitleToDisplay = FString("");
	}
};
