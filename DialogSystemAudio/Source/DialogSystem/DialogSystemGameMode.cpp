// Copyright Epic Games, Inc. All Rights Reserved.

#include "DialogSystemGameMode.h"
#include "DialogSystemCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADialogSystemGameMode::ADialogSystemGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
