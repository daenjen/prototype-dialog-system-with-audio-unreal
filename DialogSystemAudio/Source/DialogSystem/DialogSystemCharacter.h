// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "DialogSystem/NPC/NPCCharacter.h"
#include "DialogSystem/Data/FDialog.h"
#include "DialogSystem/UI/DialogUI.h"
#include "DialogSystemCharacter.generated.h"

UCLASS(config = Game)
class ADialogSystemCharacter : public ACharacter
{
	GENERATED_BODY()

		ADialogSystemCharacter();

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		float BaseLookUpRate;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "DialogSystem")
	//	float TimeBeforeNPCTalk = 2.f;

		//Array of questions
		UPROPERTY(BlueprintReadOnly, Category = "DialogSystem")
		TArray<FString> Questions;

	//Function to talk to the NPC
	UFUNCTION(BlueprintCallable)
		void Talk(FString MyQuestions, TArray<FSubtitle>& Subtitles);

protected:
	//Component for the dialog audio
	UPROPERTY(VisibleAnywhere)
		class UAudioComponent* AudioComp;

	//Reference of UI
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "DialogSystem")
		UDialogUI* MyUI;

	//Open or close our UI
	UFUNCTION(BlueprintImplementableEvent, Category = "DialogSystem")
		void ToggleUI();

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	//Generate player lines
	void GeneratePlayerLines(UDataTable& PlayerLines);

	//Enable or disable conversation if in talk range or not
	void SetTalkRange(bool InRange) { bIsInTalkRange = InRange; }

	//Set the NPC
	void SetNPC(ANPCCharacter* NPC) { NPCCharacter = NPC; }

	//Get our UI dialog reference
	UDialogUI* GetUI() { return MyUI; }

private:
	//Reference of the NPC
	ANPCCharacter* NPCCharacter;

	//Reference to our available lines
	UDataTable* AvailableLines;

private:
	//Check if player is talking with the npc


	//Check if player is in range to the npc
public:
	UPROPERTY(EditAnywhere)
		bool bIsTalking;

	UPROPERTY(EditAnywhere)
		bool bIsInTalkRange;

private:
	//Initiate the conversation
	void TalkToNPC();

	//Search the row in the specific Data Table
	FDialog* SearchDialog(UDataTable* TableToSearch, FName RowName);

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

